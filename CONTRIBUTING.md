# Contributing to `toolbox`

First off, thanks for taking the time to contribute !
It's people like you that move projects further :rocket:

There are plenty of ways you may be able to help out, like:
- [Creating issues](https://gitlab.com/openbits/toolbox/-/issues/new)
to report bugs or suggest new features;
- Writing & maintaining documentation;
- Spreading the word about `toolbox` and how awesome it is;

Code contributions are not accepted for now until I figure out where this
project is heading, but I do appreciate the inclination you may have towards
such contributions :thumbsup:

